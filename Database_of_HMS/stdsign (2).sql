-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 08, 2020 at 08:56 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stdsign`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `S.no` int(255) NOT NULL,
  `student` text NOT NULL,
  `doctor` text NOT NULL,
  `timing` datetime(6) NOT NULL,
  `dis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`S.no`, `student`, `doctor`, `timing`, `dis`) VALUES
(1, '181ME100', '181DR102', '2020-06-06 02:57:00.000000', '             headache and vomiting'),
(3, '181ME100', '181DR100', '0000-00-00 00:00:00.000000', '              headache and body pain'),
(4, '181ME100', '181DR100', '2020-06-08 11:52:00.000000', '              head ache'),
(5, '181ME120', '181DR100', '2020-06-08 12:02:00.000000', '              head ache');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `S.no` int(255) NOT NULL,
  `name` text NOT NULL,
  `Branch` text NOT NULL,
  `phone` int(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `insti_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` char(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`S.no`, `name`, `Branch`, `phone`, `email`, `insti_id`, `password`, `status`) VALUES
(1, 'Dr ABG', 'OPD specialist', 912345671, 'drabc@nitk.edu.in', '181DR100', 'qwerty123', 'on'),
(2, 'Dr rajesh', 'neuro', 912345679, 'rajest@nitk.edu.in', '181DR102', 'qwerty123', 'off'),
(3, 'Dr yuva', 'OPD specialist', 2147483647, 'yuva@nitk.edu.in', '181DR110', 'qwerty123', NULL),
(4, 'Dr Rahul', 'gastrologist', 2147483647, 'rahul@nitk.edu.in', '181DR120', 'qwerty123', NULL),
(6, 'dr tiwari', 'OPD specialist', 2147483647, 'tiwari@nitk.edu.in', '181DR130', 'qwerty123', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `medical`
--

CREATE TABLE `medical` (
  `S.no` int(255) NOT NULL,
  `stu_id` varchar(255) NOT NULL,
  `name_s` text NOT NULL,
  `doc_id` varchar(255) NOT NULL,
  `name_d` text NOT NULL,
  `reason` varchar(255) NOT NULL,
  `medc` varchar(255) NOT NULL,
  `aptime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `medical`
--

INSERT INTO `medical` (`S.no`, `stu_id`, `name_s`, `doc_id`, `name_d`, `reason`, `medc`, `aptime`) VALUES
(1, '181ME100', 'ABCD', '181DR102', 'Dr rajesh', '                kuch toh hua hai', 'upload/rot.jpg', '2020-06-07 15:25:00.000000'),
(4, '181ME100', 'ABCD', '181DR102', 'Dr rajesh', '                fell ill', 'upload/screencapture-localhost-dashboard-php-2020-06-07-19_53_47.png', '2020-06-08 11:40:00.000000'),
(5, '181ME100', 'ABCD', '181DR102', 'Dr rajesh', '                fracture', 'upload/screencapture-localhost-dashboard2-php-2020-06-07-20_36_41.png', '2020-06-08 11:50:00.000000'),
(6, '181ME120', 'ABC', '181DR130', 'dr tiwari', '                fracture', 'upload/screencapture-localhost-dashboard1-php-2020-06-07-19_30_05.png', '2020-06-08 12:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `others`
--

CREATE TABLE `others` (
  `S.no` int(255) NOT NULL,
  `name` text NOT NULL,
  `Branch` text NOT NULL,
  `phone` int(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `insti_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `others`
--

INSERT INTO `others` (`S.no`, `name`, `Branch`, `phone`, `email`, `insti_id`, `password`) VALUES
(2, 'nancy', 'reception', 2147483647, 'nancy@nitk.edu.in', '181RE100', 'qwerty123');

-- --------------------------------------------------------

--
-- Table structure for table `pres`
--

CREATE TABLE `pres` (
  `S.no` int(255) NOT NULL,
  `stu_id` varchar(255) NOT NULL,
  `name_s` text NOT NULL,
  `doc_id` varchar(255) NOT NULL,
  `name_d` text NOT NULL,
  `Symcon` varchar(255) NOT NULL,
  `Pres` varchar(255) NOT NULL,
  `tim_dat` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pres`
--

INSERT INTO `pres` (`S.no`, `stu_id`, `name_s`, `doc_id`, `name_d`, `Symcon`, `Pres`, `tim_dat`) VALUES
(1, '181ME100', 'ABCD', '181DR100', 'Dr ABC', 'Sym:Abdomin pain and headache.\r\nCon:Dyrrea', 'Tablet A-Thrice a day after food\r\nSyrup b- thrice a day after food ', '2020-06-07 09:18:00.000000'),
(2, '181ME100', 'ABCD', '181DR102', 'Dr rajesh', 'sym: vomiting\r\ncon: dyrrea', '            Tablet A- thrice a day\r\nSyrus- thrcie a day    ', '2020-06-07 12:10:00.000000'),
(3, '181ME120', 'ABC', '181DR102', 'Dr rajesh', '                sym: headache and bodyache \r\n                con: viral fever', '          Tablet-c - thrice a day\r\n          Glucose- twice a day', '2020-06-07 17:14:00.000000'),
(7, '181ME100', 'ABC', '181DR102', 'Dr rajesh', '                sym: xyz\r\ncon: abc', '                tablet a\r\nsyrus b', '2020-06-08 11:38:00.000000'),
(8, '181ME100', 'ABCD', '181DR102', 'Dr rajesh', '                sym: headache and body pain\r\ncon: viral fever', '                tablet 1\r\nsyrus 2', '2020-06-08 11:48:00.000000'),
(9, '181ME120', 'ABC', '181DR130', 'dr tiwari', '                sym: headache and bodypain\r\ncon: viral fever', '                tablet-1\r\nsyrus -3 both thrice a day', '2020-06-08 11:59:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `S.no` int(11) NOT NULL,
  `name` text NOT NULL,
  `Branch` text NOT NULL,
  `phone` bigint(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `insti_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`S.no`, `name`, `Branch`, `phone`, `email`, `insti_id`, `password`) VALUES
(2, 'ABCD', 'mech', 9408888888, 'abc@nitk.edu.in', '181ME100', 'qwerty123'),
(1, 'ABC', 'mech', 9403588888, 'abc@nitk.edu.in', '181ME120', 'qwerty123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD UNIQUE KEY `S.no` (`S.no`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`insti_id`),
  ADD UNIQUE KEY `S.no` (`S.no`);

--
-- Indexes for table `medical`
--
ALTER TABLE `medical`
  ADD UNIQUE KEY `S.no` (`S.no`);

--
-- Indexes for table `others`
--
ALTER TABLE `others`
  ADD PRIMARY KEY (`insti_id`),
  ADD UNIQUE KEY `S.no` (`S.no`);

--
-- Indexes for table `pres`
--
ALTER TABLE `pres`
  ADD UNIQUE KEY `S.no` (`S.no`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`insti_id`),
  ADD UNIQUE KEY `S.no` (`S.no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `S.no` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `S.no` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `medical`
--
ALTER TABLE `medical`
  MODIFY `S.no` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `others`
--
ALTER TABLE `others`
  MODIFY `S.no` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pres`
--
ALTER TABLE `pres`
  MODIFY `S.no` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `S.no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
