<!DOCTYPE html>
<html lang="en">

<head>
    <title>demo IRIS HC portal</title>
        <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="node_modules/bootstrap-social/bootstrap-social.css">
</head>

<body>
    <nav class="navbar navbar-dark navbar-expand-sm  fixed-top">
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#Navbar">
                <span class="navbar-toggler-icon"></span>
            </button>
               <a class="navbar-brand mr-auto" href="#"><img src="img\IRIS.png" width="41" height="30" class="z-depth-3 rounded"></a>
                    <div class="collapse navbar-collapse" id="Navbar">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active"><a class="nav-link" href="#"><span class="fa fa-home fa-lg"></span> Dashboard</a></li>
                            <li class="nav-item"><a class="nav-link" href="./aboutus.html"><span class="fa fa-info fa-lg"></span>Health Info</a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><span class="fa fa-list fa-lg"></span> Updates</a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><span class="fa fa-address-card fa-lg"></span> Contact</a></li>
                        </ul>
                        
                        <dl class="row">
                            <dt class="navbar-item col-6 align-self-center"><a class="nav-link" href="#"><span class="fa fa-user-circle fa-lg">Profile</span></a></dt>
                            <dt class="navbar-item col-2 align-self-center"><a class="nav-link" href="#"><img src="img\NITK.png" width="41" height="30" class="z-depth-3 rounded"></a></dt>
                        </dl>
                        
                    </div>
        </div>
    </nav>
    <header class="jumbotron">
        <Div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#Navbar">
                <span class="navbar-toggler-icon"></span>
                <div class="collapse navbar-collapse" id="Navbar">
                
            </button>
            <div class="row ">
                <div class="col-12 col-sm-6">
                    <h1>IRIS Health Center portal</h1>
                    <p>NITK takes another step to live up to its technical expertise ,by bringing NITK Health Care centre to a touch of your screen.
                    A portal dedicated by IRIS to HC NITK.</p>
                </div >
                <div class="col col-sm-6 align-self-center">
                    <img src="img\IRIS.png" class="rounded z-depth-3">
                </div>
            </div>
        </div>
    </header>

    <div class="container">
        <div class="row row-content align-item-center offset-4">
           <div class="card mb-10 order-sm-first offset-1" style="width: 18rem;box-shadow: 5px 10px 8px #888888;">
                <img class="card-img-top" src="img/HCC.jpg" alt="Card image cap">
                <div class="card-body">
                <h5 class="card-title">Doctor Login</h5>
                <p class="card-text">If you are a doctor at NITK Health Center, click on the given button to quickly login.</p>
                <a href="login.php" class=" nav-link btn btn-primary" style="width: 60%">Login</a>
                </div>
            </div>
            <div class="card mb-10 order-sm-last offset-4" style="width: 18rem;box-shadow: 5px 10px 8px #888888;">
                <img class="card-img-top" src="img/rec.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Receptionist login</h5>
                    <p class="card-text">If you are a receptionist at NITK Health Center, click on the given button to quickly login.</p>
                    <a href="./login2.php" class="nav-link btn btn-primary" style="width: 60%">login</a>
                </div>
            </div> 
        </div>


        <div class="row row-content align-item-center offset-4">
           <div class="card mb-10 order-sm-first offset-5" style="width: 18rem;box-shadow: 5px 10px 8px #888888;">
              <img class="card-img-top " src="img/stu.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Student login</h5>
                    <p class="card-text">If you are a doctor at NITK Health Center, click on the given button to quickly login.</p>
                    <a href="login1.php" class="nav-link btn btn-primary"style="width: 60%">login</a>
                </div>
            </div>
            
        </div>

        

        <div class="row row-content justify-content-center ">
           <div class="card border-info img1  text-center">
              <div class="card-header">
                New to Health Centre IRIS!!
              </div>
              <div class="card-body text-info">
                <h5 class="card-title">Sign up to HC IRIS</h5>
                <p class="card-text">If you are a new resident of NITK or haven't join HC IRIS.<br> Kindly Sign up using college credentials</p>
                <a href="./Signup_student1.php" class="btn btn-success nav-link">Sign Up</a>
                <hr>
                <p class="align-self-center text-center">
                <div class="btn btn-danger" href="#">Google+</div>
                </p>
              </div>
              <div class="card-footer text-muted">
              </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <div class="row">             
                <div class=" col-4 offset-1 col-sm-2">
                    <h5>Links</h5>
                    <ul class="list-unstyled">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Menu</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
                <div class="col-6 col-sm-5">
                    <h5>Our Address</h5>
                    <address>
                      575025- srinivasnagar<br>
                      Dakshin kannada, Mangalore<br>
                      karnataka, India<br>
		              <i class="fa fa-phone fa-lg"></i>Phone: +852 1234 5678<br>
                      <i class="fa fa-fax fa-lg"></i>Fax: +852 8765 4321<br>
                      <i class="fa fa-envelope fa-lg"></i>E-mail: 
                      <a href="mailto:confusion@food.net">confusion@food.net</a>

		           </address>
                </div>
                <div class="col col-sm-4 align-self-center ">
                    <div class="text-center">
                        <a class="btn btn-social-icon btn-google" href="http://google.com/+"><i class="fa fa-google-plus"></i></a>
                        <a class="btn btn-social-icon btn-facebook" href="http://www.facebook.com/profile.php?id="><i class="fa fa-facebook"></i></a>
                        <a class="btn btn-social-icon btn-linkedin" href="http://www.linkedin.com/in/"><i class="fa fa-linkedin"></i></a>
                        <a class="btn btn-social-icon btn-twitter" href="http://twitter.com/"><i class="fa fa-twitter"></i></a>
                        <a class="btn btn-social-icon btn-google" href="http://youtube.com/"><i class="fa fa-youtube"></i></a>
                        <a class="btn btn-social-icon" href="mailto:"><i class="fa fa-envelope-o"></i></a>
                    </div>

                </div>
           </div>
           <div class="row justify-content-center">             
                <div class="col-auto">
                    <p>© Copyright 2020 IRIS</p>
                </div>
           </div>
        </div>
    </footer>
    <!-- jQuery first, then Popper.js, then Bootstrap JS. -->
    <script src="node_modules/jquery/dist/jquery.slim.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
</body>

</html>