 <?php
include_once 'database.php';
?>
<?php
 if(isset($_GET['upd'])){
    $id = $_GET['upd'];
    $query = "SELECT * FROM doctor WHERE insti_id='$id'";
    $fire = mysqli_query($conn,$query) or die("can not fetch the data".mysqli_error($conn));
    $user = mysqli_fetch_assoc($fire);
    echo $user['name'];
 }

?>
<?php
    if(isset($_POST['btnupdate'])){
        $name = $_POST['name'];
        $Branch= $_POST['Branch'];
        $phone = $_POST['phone'];
        $email = $_POST['email'];
        $insti_id = $_POST['insti_id'];

        $query="UPDATE doctor SET name = '$name', Branch = '$Branch', phone = '$phone', email = '$email', insti_id = '$insti_id' WHERE insti_id='$id'";
        $fire = mysqli_query($conn,$query) or die(" can not update the data".mysql_error($conn));
        if($fire) {echo "Data succesfully updated";
            header("Location: dashboard2.php");
           
    }
}

?>
 <!DOCTYPE html>
<html lang="en">

<head>

    <title>demo IRIS HC portal</title>
        <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
   

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" 
      href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" 
      integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"crossorigin="anonymous">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="node_modules/bootstrap-social/bootstrap-social.css">
</head>

<body>
 <div class="container jumbotron  " style="margin: 10em;">
         <div class="row row-content justify-content-center">
           <div class="col-12">
              <h3 style="text-align: center; font-size: 3em; margin-bottom: 2em;">Update record of HCC</h3>
           </div>
            <div class="col-12 col-md-9">
                <form action="<?php $_SERVER['PHP_SELF']?>" method="POST">
                    <div class="form-group row">
                        <label for="name" class="col-md-2 col-form-label"> Name</label>
                        <div class="col-md-10">
                            <input value="<?php echo $user['name'] ?>" type="text" class="form-control" id="name" name="name" placeholder="Name" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="Branch" class="col-md-2 col-form-label">Branch/post</label>
                        <div class="col-md-10">
                            <input value="<?php echo $user['Branch'] ?>" type="text" class="form-control" id="Branch" name="Branch" placeholder="Branch" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="phone" class="col-12 col-md-2 col-form-label">Contact Tel.</label>
                        <div class="col-5 col-md-3">
                            <input type="number" class="form-control" id="areacode" name="areacode" placeholder="Area code(Optional)">
                        </div>
                        <div class="col-7 col-md-7">
                            <input value="<?php echo $user['phone'] ?>" type="number" class="form-control" id="phone" name="phone" placeholder="Tel. number">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-md-2 col-form-label">Email</label>
                        <div class="col-md-10">
                            <input value="<?php echo $user['email'] ?>" type="email" class="form-control" id="email" name="email" placeholder="Email @nitk.edu.in">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="insti_id" class="col-md-2 col-form-label">Institute ID(Roll No)</label>
                        <div class="col-md-10">
                            <input value="<?php echo $user['insti_id'] ?>" type="text" class="form-control" id="insti_id" name="insti_id" placeholder="181XX290 or Institute Id number">
                        </div>
                    </div>
                    
                    
                
                    <div class="form-group row">
                        <div class="offset-md-2 col-md-10">
                            <input  type="submit" class="btn btn-warning align-self-center" name="btnupdate" id="btnupdate" value="Update" style="width: 30%;">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container">
            <div class="row">             
                <div class=" col-4 offset-1 col-sm-2">
                    <h5>Links</h5>
                    <ul class="list-unstyled">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Menu</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
                <div class="col-6 col-sm-5">
                    <h5>Our Address</h5>
                    <address>
                      575025- srinivasnagar<br>
                      Dakshin kannada, Mangalore<br>
                      karnataka, India<br>
                      <i class="fa fa-phone fa-lg"></i>Phone: +852 1234 5678<br>
                      <i class="fa fa-fax fa-lg"></i>Fax: +852 8765 4321<br>
                      <i class="fa fa-envelope fa-lg"></i>E-mail: 
                      <a href="mailto:confusion@food.net">confusion@food.net</a>

                   </address>
                </div>
                <div class="col col-sm-4 align-self-center ">
                    <div class="text-center">
                        <a class="btn btn-social-icon btn-google" href="http://google.com/+"><i class="fa fa-google-plus"></i></a>
                        <a class="btn btn-social-icon btn-facebook" href="http://www.facebook.com/profile.php?id="><i class="fa fa-facebook"></i></a>
                        <a class="btn btn-social-icon btn-linkedin" href="http://www.linkedin.com/in/"><i class="fa fa-linkedin"></i></a>
                        <a class="btn btn-social-icon btn-twitter" href="http://twitter.com/"><i class="fa fa-twitter"></i></a>
                        <a class="btn btn-social-icon btn-google" href="http://youtube.com/"><i class="fa fa-youtube"></i></a>
                        <a class="btn btn-social-icon" href="mailto:"><i class="fa fa-envelope-o"></i></a>
                    </div>

                </div>
           </div>
           <div class="row justify-content-center">             
                <div class="col-auto">
                    <p>© Copyright 2020 IRIS</p>
                </div>
           </div>
        </div>
    </footer>
    <!-- jQuery first, then Popper.js, then Bootstrap JS. -->
    <script src="node_modules/jquery/dist/jquery.slim.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
</body>


</html>
